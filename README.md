helm install my-app my-app-chart

helm upgrade meu-ingress ./meu-chart-ingress

helm upgrade --dry-run --debug meu-ingress ./meu-chart-ingress

helm upgrade --reuse-values meu-ingress ./meu-chart-ingres



# README.md

## Descrição
Este repositório contém os arquivos necessários para a instalação e atualização de uma aplicação chamada `my-app` utilizando o Helm no GitLab. O Helm é uma ferramenta de gerenciamento de pacotes para Kubernetes.

## Instruções de Uso

### Instalação da Aplicação
Para criar o helm `helm-api-store`, utilize o seguinte comando:

```bash
helm install helm-api-store ./chart-api-store
```

### O parâmetro --dry-run simula a atualização sem modificar o estado real, e --debug imprime informações detalhadas para debug.
```bash
helm upgrade --dry-run --debug helm-api-store .
```

### Este comando atualiza o chart do Ingress chamado helm-api-store e aplica as mudanças no cluster Kubernetes.
```bash
helm upgrade helm-api-store ./chart-api-store
```

### Este comando reutiliza os valores existentes ao atualizar os recursos, mantendo as configurações previamente definidas.
```bash
helm upgrade --reuse-values helm-api-store ./chart-api-store
```



base_url="http://127.0.0.1:8000"; for i in {1..500}; do nome_loja="loja$i"; url_loja="loja$i.omeuingresso.com.br"; ./lojas.bash add "$nome_loja" "$url_loja"; done
