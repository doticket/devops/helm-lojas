#!/bin/bash

VALUES_FILE="/home/anderson/DEV/doTicket-V2/ESTUDO/chart-api-store/values.yaml"

add_loja() {
  local id=$1
  local url_loja=$2

  # Adiciona uma nova entrada ao campo "lojas" com ID específico
  echo "  - id: $id" >> "$VALUES_FILE"
  echo "    host: \"$url_loja\"" >> "$VALUES_FILE"
}

remove_loja() {
  local id=$1

  # Remove a entrada do campo "lojas" com base no ID da loja e a linha seguinte
  awk -v id="$id" '/- id: [0-9]+$/ {if ($3 == id) {getline; next}} 1' "$VALUES_FILE" > "$VALUES_FILE.tmp" && mv "$VALUES_FILE.tmp" "$VALUES_FILE"
}

update_loja() {
  local id=$1
  local new_url=$2
  awk -v id="$id" '/- id: [0-9]+$/ {if ($3 == id) {getline; next}} 1' "$VALUES_FILE" > "$VALUES_FILE.tmp" && mv "$VALUES_FILE.tmp" "$VALUES_FILE"
  echo "  - id: $id" >> "$VALUES_FILE"
  echo "    host: \"$new_url\"" >> "$VALUES_FILE"

}

if [ "$1" == "add" ]; then
  add_loja "$2" "$3"
elif [ "$1" == "remove" ]; then
  remove_loja "$2"
elif [ "$1" == "update" ]; then
  update_loja "$2" "$3"
else
  echo "Uso: $0 add <id> <url_api_loja>"
  echo "     $0 remove <id>"
  echo "     $0 update <id> <url_api_loja>"
  exit 1
fi
